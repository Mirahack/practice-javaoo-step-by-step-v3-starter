package ooss;

import java.util.ArrayList;
import java.util.List;

public class Teacher extends Person {

    private List<Klass> klasses;

    public Teacher(Integer id, String name, Integer age) {
        super(id, name, age);
        klasses = new ArrayList<>();
    }

    @Override
    public String introduce() {
        if (klasses.size() == 0) {
            return String.format("My name is %s. I am %d years old. I am a teacher.", name, age);
        } else {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(" I teach Class ");
            for (Klass klass : klasses) {
                stringBuilder.append(klass.getNumber()).append(", ");
            }
            return String.format("My name is %s. I am %d years old. I am a teacher.", name, age) + stringBuilder.substring(0, stringBuilder.length() - 2) + ".";
        }
    }

    public void assignTo(Klass klass) {
        if (klass != null && (!this.klasses.contains(klass))) {
            this.klasses.add(klass);
        }
    }

    public boolean belongsTo(Klass klass) {
        return this.klasses.contains(klass);
    }

    public boolean isTeaching(Student tom) {
        return this.klasses.contains(tom.getKlass());
    }

    @Override
    public void notified(Klass klass, Student king) {
        System.out.printf("I am %s, teacher of Class %d. I know %s become Leader.%n", this.name, klass.getNumber(), king.name);
    }
}
