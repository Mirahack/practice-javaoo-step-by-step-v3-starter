package ooss;

import java.util.Objects;

public class Person {

    protected Integer id;

    protected String name;

    protected Integer age;

    public Person(Integer id, String name, Integer age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public String introduce() {
        return String.format("My name is %s. I am %d years old.", name, age);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Person anotherPerson = (Person) obj;
        return this.id.equals(anotherPerson.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public void notified(Klass klass, Student king) {
        System.out.printf("I am %s, some person of Class %d. I know %s become Leader.%n", this.name, klass.getNumber(), king.name);
    }
}
