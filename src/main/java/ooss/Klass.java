package ooss;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Klass {

    private int number;

    private Student leader;

    private Set<Teacher> teacherAttachmentSet = new HashSet<>();

    private Set<Student> studentAttachmentSet = new HashSet<>();

    public Klass(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Klass anotherKlass = (Klass) obj;
        return anotherKlass.number == this.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    public void assignLeader(Student king) {
        if (king != null && king.isIn(this)) {
            this.leader = king;
            notifyAllObservers(king);
        } else {
            System.out.println("It is not one of us.");
        }
    }

    private void notifyAllObservers(Student king) {
        for (Teacher teacher : this.teacherAttachmentSet) {
            teacher.notified(this, king);
        }
        for (Student student : this.studentAttachmentSet) {
            student.notified(this, king);
        }
    }

    public boolean isLeader(Student tom) {
        if (tom == null || this.leader == null) {
            return false;
        } else {
            return leader.equals(tom);
        }
    }

    public void attach(Teacher jerry) {
        if (jerry != null) {
            teacherAttachmentSet.add(jerry);
        }
    }

    public void attach(Student bob) {
        if (bob != null) {
            studentAttachmentSet.add(bob);
        }
    }

    public void disAttach(Teacher jerry) {
        teacherAttachmentSet.remove(jerry);
    }

    public void disAttach(Student bob) {
        studentAttachmentSet.remove(bob);
    }
}
