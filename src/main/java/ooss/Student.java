package ooss;

public class Student extends Person {

    private Klass klass;

    public Student(Integer id, String name, Integer age) {
        super(id, name, age);
    }

    public Klass getKlass() {
        return klass;
    }

    @Override
    public String introduce() {
        if (this.klass == null) {
            return String.format("My name is %s. I am %d years old. I am a student.", name, age);
        } else if (this.klass.isLeader(this)) {
            return String.format("My name is %s. I am %d years old. I am a student. I am the leader of class %d.", name, age, this.klass.getNumber());
        } else {
            return String.format("My name is %s. I am %d years old. I am a student. I am in class %d.", name, age, klass.getNumber());
        }
    }

    public void join(Klass klass) {
        this.klass = klass;
    }

    public boolean isIn(Klass klass) {
        if (this.klass == null) {
            return false;
        } else {
            return this.klass.equals(klass);
        }
    }

    @Override
    public void notified(Klass klass, Student king) {
        System.out.printf("I am %s, student of Class %d. I know %s become Leader.%n", this.name, klass.getNumber(), king.name);
    }
}
