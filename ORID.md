O:This morning, we conducted a code review first, and during the review process, our team conducted code mutual checks. Afterwards, we learned the Stream API and did some related exercises.
In the afternoon, we were all doing exercises related to object orientation. The teacher first introduced the relevant knowledge of object-oriented, and then we did a lot of exercises on object-oriented.

R:I feel tired but satisfied.

I:Although I have been familiar with the concept of object-oriented for a long time and have implemented it in Java many times, I still consolidated and filled in many knowledge gaps in today's course. I spent most of today writing code, which made me feel a bit tired, but the final result was satisfactory.

D:Today's class has given me a deeper understanding of the three major characteristics of object-oriented language, and I believe that this knowledge will lay a solid foundation for my future use of object-oriented languages.